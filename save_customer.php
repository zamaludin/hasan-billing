<?php
include 'database.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = $_POST['name'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $whatsapp = $_POST['whatsapp'];
    $job = $_POST['job'];
    $identity = $_POST['identity'];
    $package = $_POST['package'];
    $lat = $_POST['lat'];
    $lon = $_POST['lon'];

    $sql = "INSERT INTO customers (name, birthdate, address, phone, whatsapp, job, identity, package, lat, lon)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssssssssdd", $name, $birthdate, $address, $phone, $whatsapp, $job, $identity, $package, $lat, $lon);

    if ($stmt->execute() === TRUE) {
        echo "New customer added successfully";
    } else {
        echo "Error: " . $stmt->error;
    }

    $stmt->close();
}
$conn->close();
?>
