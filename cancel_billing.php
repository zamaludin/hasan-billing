<?php
include 'database.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $customer_id = $_POST['customer_id'];

    // Update billing status to unpaid in the database
    $sql = "UPDATE bills SET status = 0 WHERE customer_id = ? AND month = MONTH(CURRENT_DATE)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $customer_id);

    if ($stmt->execute()) {
        echo "success";
    } else {
        echo "Failed to update billing status.";
    }
}
?>
