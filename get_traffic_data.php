<?php
require_once 'mikrotik_connection.php';

header('Content-Type: application/json');

try {
    $interface = getenv('MIKROTIK_INTERFACE');
    $trafficData = getTrafficData($interface);

    $rx = isset($trafficData[0]['rx-bits-per-second']) ? $trafficData[0]['rx-bits-per-second'] / 1000000 : 0;
    $tx = isset($trafficData[0]['tx-bits-per-second']) ? $trafficData[0]['tx-bits-per-second'] / 1000000 : 0;

    echo json_encode([
        'rx' => $rx,
        'tx' => $tx,
    ]);

} catch (Exception $e) {
    echo json_encode(['error' => $e->getMessage()]);
    exit;
}
?>
