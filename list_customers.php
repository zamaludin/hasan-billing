<?php
require_once 'database.php';

$customers = getAllCustomers();

function getAllCustomers() {
    global $conn;
    $sql = "SELECT * FROM customers";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}
?>


    <h2 class="mt-5">List of Customers</h2>
    <a href="add_customer.php" class="btn btn-primary mb-3">Add Customer</a>
    <table id="customersTable" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Birthdate</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Whatsapp</th>
                <th>Job</th>
                <th>Identity</th>
                <th>Package</th>
                <th>Registration Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $customer): ?>
                <tr>
                    <td><?php echo htmlspecialchars($customer['name'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['birthdate'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['address'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['phone'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['whatsapp'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['job'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['identity'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['package_id'] ?? ''); ?></td>
                    <td><?php echo htmlspecialchars($customer['registration_date'] ?? ''); ?></td>
                    <td>
                        <a href="edit_customer.php?id=<?php echo $customer['customer_id']; ?>" class="btn btn-warning btn-sm">Edit</a>
                        <a href="delete_customer.php?id=<?php echo $customer['customer_id']; ?>" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script>
    $.noConflict();

    jQuery(document).ready(function($) {
        $('#customersTable').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true
        });
    });
</script>
</body>
</html>
