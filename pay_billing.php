<?php
include 'database.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $customer_id = $_POST['customer_id'];

    // Insert or update billing status in the database
    $sql = "INSERT INTO bills (customer_id, month, status) VALUES (?, MONTH(CURRENT_DATE), 1)
            ON DUPLICATE KEY UPDATE status = 1";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $customer_id);

    if ($stmt->execute()) {
        echo "success";
    } else {
        echo "Failed to update billing status.";
    }
}
?>
