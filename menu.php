<?php
if (!isset($_SESSION)) { 
    session_start(); 
}
if (!isset($_SESSION['admin'])) {
    header("Location: login.php");
    exit();
}
?>
<!-- Sidebar -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" role="menu">
                <li class="nav-item">
                    <a href="admin.php?page=dashboardbilling.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'dashboardbilling.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard Billing</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin.php?page=add_customer.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'add_customer.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-user-plus"></i>
                        <p>Add Customer</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin.php?page=list_customers.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'list_customers.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>List Customers</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin.php?page=list_packages.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'list_packages.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-box"></i>
                        <p>Manage Packages</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin.php?page=pppoe_list.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'pppoe_list.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>List PPPoE</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin.php?page=realtime_traffic.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'realtime_traffic.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>Realtime Traffic</p>
                     </a>
                    
                </li>
                <li class="nav-item">
                    <a href="admin.php?page=ping_pppoe.php" class="nav-link <?php echo isset($_GET['page']) && $_GET['page'] == 'ping_pppoe.php' ? 'active' : ''; ?>">
                        <i class="nav-icon fas fa-network-wired"></i>
                        <p>Ping PPPoE Users</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
