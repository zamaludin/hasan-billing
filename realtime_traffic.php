<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Realtime Traffic Monitoring</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .table-hover tbody tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
<div class="container">
    <h2 class="mt-5">Realtime Traffic Monitoring</h2>
    <canvas id="trafficChart" width="400" height="200"></canvas>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-streaming@2.0.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-date-fns"></script>
<script>
    const ctx = document.getElementById('trafficChart').getContext('2d');
    const trafficChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                label: 'Download (Mbps)',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
                fill: false,
                data: []
            }, {
                label: 'Upload (Mbps)',
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1,
                fill: false,
                data: []
            }]
        },
        options: {
            scales: {
                x: {
                    type: 'realtime',
                    realtime: {
                        delay: 2000,
                        refresh: 1000,
                        onRefresh: function(chart) {
                            fetch('get_traffic_data.php')
                                .then(response => response.json())
                                .then(data => {
                                    if (!data.error) {
                                        const now = Date.now();
                                        chart.data.datasets[0].data.push({
                                            x: now,
                                            y: data.rx
                                        });
                                        chart.data.datasets[1].data.push({
                                            x: now,
                                            y: data.tx
                                        });
                                        chart.update('quiet');
                                    } else {
                                        console.error(data.error);
                                    }
                                })
                                .catch(error => console.error('Error fetching data:', error));
                        }
                    }
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Speed (Mbps)'
                    }
                }
            }
        }
    });
</script>
</body>
</html>
