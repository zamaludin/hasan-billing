<?php
require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'mikrotik_connection.php';

$username = $_GET['username'];
$ip = getPPPoEUserIP($username);

$result = [
    'username' => $username,
    'status' => 'Inactive',
];

if ($ip) {
    $result['status'] = pingAddress($ip) ? 'Active' : 'Inactive';
}

header('Content-Type: application/json');
echo json_encode($result);
?>
