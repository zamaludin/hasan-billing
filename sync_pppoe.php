<?php
require_once 'mikrotik_connection.php';
require_once 'database.php';

try {
    $data = getPPPoEData();
    savePPPoEData($data);
    echo "PPPoE data synchronized successfully.";
} catch (Exception $e) {
    echo "Failed to sync PPPoE data: ", $e->getMessage();
}
?>
