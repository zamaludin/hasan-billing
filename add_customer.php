<?php
include 'database.php';
$packages = getAllPackages();
?>

<h1 class="my-4">Add New Customer</h1>
<form action="save_customer.php" method="post">
    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" class="form-control" id="name" name="name" required>
    </div>
    <div class="form-group">
        <label for="birthdate">Birth Date:</label>
        <input type="date" class="form-control" id="birthdate" name="birthdate" required>
    </div>
    <div class="form-group">
        <label for="address">Address:</label>
        <input type="text" class="form-control" id="address" name="address" required>
    </div>
    <div class="form-group">
        <label for="phone">Phone:</label>
        <input type="text" class="form-control" id="phone" name="phone" required>
    </div>
    <div class="form-group">
        <label for="whatsapp">WhatsApp:</label>
        <input type="text" class="form-control" id="whatsapp" name="whatsapp" required>
    </div>
    <div class="form-group">
        <label for="job">Job:</label>
        <input type="text" class="form-control" id="job" name="job" required>
    </div>
    <div class="form-group">
        <label for="identity">Identity Number:</label>
        <input type="text" class="form-control" id="identity" name="identity" required>
    </div>
    <div class="form-group">
        <label for="package_id">Package:</label>
        <select class="form-control" id="package_id" name="package_id" required>
            <?php foreach ($packages as $package): ?>
                <option value="<?php echo $package['package_id']; ?>"><?php echo $package['name'] . ' - ' . $package['speed'] . ' - ' . number_format($package['price']); ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="lat">Latitude:</label>
        <input type="text" class="form-control" id="lat" name="lat" required>
    </div>
    <div class="form-group">
        <label for="lon">Longitude:</label>
        <input type="text" class="form-control" id="lon" name="lon" required>
    </div>
    <button type="submit" class="btn btn-primary">Add Customer</button>
    <?php if (isset($error)): ?>
        <p class="text-danger mt-3"><?php echo $error; ?></p>
    <?php endif; ?>
</form>
