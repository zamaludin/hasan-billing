<?php
include 'database.php';

$id = $_GET['id'];

if (deletePackage($id)) {
    header("Location: list_packages.php");
    exit();
} else {
    echo "Failed to delete package.";
}
?>
