<?php
require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'mikrotik_connection.php';

// Ambil data PPPoE dari database
$secrets = getAllPPPoEUsers();

try {
    // Ambil daftar koneksi aktif dari Mikrotik
    $activeConnections = getActivePPPoEData();
    $activeNames = array_column($activeConnections, 'name');
} catch (Exception $e) {
    $activeNames = [];
    echo "Failed to fetch active connections: ", $e->getMessage();
}

$totalUsers = count($secrets);
$activeUsers = count(array_filter($secrets, function($secret) use ($activeNames) {
    return in_array($secret['name'], $activeNames);
}));
$inactiveUsers = $totalUsers - $activeUsers;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PPPoE Users List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <style>
        .table-hover tbody tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
<div class="container">
    <h2 class="mt-5">Daftar Pengguna PPPoE</h2>
    <div class="mb-3">
        <button id="syncButton" class="btn btn-primary">Sinkronisasi Data PPPoE</button>
    </div>
    <div class="mb-3">
        <p><strong>Total Users:</strong> <?php echo $totalUsers; ?></p>
        <p><strong>Active Users:</strong> <?php echo $activeUsers; ?></p>
        <p><strong>Inactive Users:</strong> <?php echo $inactiveUsers; ?></p>
    </div>
    <table id="pppoeTable" class="table table-striped table-hover mt-3">
        <thead>
            <tr>
                <th>Username</th>
                <th>Service</th>
                <th>Profile</th>
                <th>Comment</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($secrets as $secret): ?>
                <tr>
                    <td><?php echo htmlspecialchars($secret['name']); ?></td>
                    <td><?php echo htmlspecialchars($secret['service']); ?></td>
                    <td><?php echo htmlspecialchars($secret['profile']); ?></td>
                    <td><?php echo htmlspecialchars($secret['comment'] ?? ""); ?></td>
                    <td>
                        <?php if (in_array($secret['name'], $activeNames)): ?>
                            <span class="badge badge-success">Active</span>
                        <?php else: ?>
                            <span class="badge badge-danger">Not Active</span>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script>
$.noConflict();

jQuery(document).ready(function($) {
        $('#pppoeTable').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });

        $('#syncButton').on('click', function() {
            $.ajax({
                url: 'sync_pppoe.php',
                method: 'GET',
                success: function(response) {
                    alert(response);
                    location.reload(); // Reload the page to show updated data
                },
                error: function() {
                    alert('Failed to sync data.');
                }
            });
        });
    });
</script>
</body>
</html>
