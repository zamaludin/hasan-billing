<?php
require_once 'vendor/autoload.php';
require_once 'database.php';

// Ambil data PPPoE dari database
$secrets = getAllPPPoEUsers();
$totalUsers = count($secrets);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PPPoE Connection Status</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .table-hover tbody tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
<div class="container">
    <h2 class="mt-5">Connection Status Pengguna PPPoE</h2>
    <div class="mb-3">
        <p><strong>Total Users:</strong> <?php echo $totalUsers; ?></p>
        <p><strong>Active Connection Users:</strong> <span id="activePingUsers">0</span></p>
        <p><strong>Inactive Connection Users:</strong> <span id="inactivePingUsers">0</span></p>
    </div>
    <button id="checkButton" class="btn btn-primary mb-3">Check All Users</button>
    <table class="table table-striped table-hover mt-3">
        <thead>
            <tr>
                <th>Username</th>
                <th>Service</th>
                <th>Profile</th>
                <th>Comment</th>
                <th>Connection Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($secrets as $secret): ?>
                <tr>
                    <td><?php echo htmlspecialchars($secret['name']); ?></td>
                    <td><?php echo htmlspecialchars($secret['service']); ?></td>
                    <td><?php echo htmlspecialchars($secret['profile']); ?></td>
                    <td><?php echo htmlspecialchars($secret['comment'] ?? ""); ?></td>
                    <td id="status-<?php echo htmlspecialchars($secret['name']); ?>">
                        <span class="badge badge-secondary">Checking...</span>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script>
    $(document).ready(function() {
        function updateConnectionStatus(username) {
            $.ajax({
                url: 'api_check_connection.php',
                method: 'GET',
                data: { username: username },
                success: function(response) {
                    var statusElement = $('#status-' + response.username);
                    if (response.status === 'Active') {
                        statusElement.html('<span class="badge badge-success">Active</span>');
                    } else {
                        statusElement.html('<span class="badge badge-danger">Inactive</span>');
                    }
                    updateCounts();
                }
            });
        }

        function updateCounts() {
            var activePingUsers = 0;
            var inactivePingUsers = 0;

            $('td[id^="status-"]').each(function() {
                if ($(this).find('.badge-success').length) {
                    activePingUsers++;
                } else if ($(this).find('.badge-danger').length) {
                    inactivePingUsers++;
                }
            });

            $('#activePingUsers').text(activePingUsers);
            $('#inactivePingUsers').text(inactivePingUsers);
        }

        $('#checkButton').on('click', function() {
            <?php foreach ($secrets as $secret): ?>
                updateConnectionStatus('<?php echo $secret['name']; ?>');
            <?php endforeach; ?>
        });
    });
</script>
</body>
</html>
