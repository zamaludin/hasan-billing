<?php
include 'database.php';
$packages = getAllPackages();
?>

<h1 class="my-4">List of Packages</h1>
<a href="admin.php?page=create_package.php" class="btn btn-primary mb-3">Create New Package</a>
<table id="packages" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Speed</th>
            <th>Price</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($packages as $package): ?>
            <tr>
                <td><?php echo $package['package_id']; ?></td>
                <td><?php echo $package['name']; ?></td>
                <td><?php echo $package['speed']; ?></td>
                <td><?php echo number_format($package['price']); ?></td>
                <td>
                    <a href="admin.php?page=update_package.php&id=<?php echo $package['package_id']; ?>" class="btn btn-warning">Edit</a>
                    <a href="admin.php?page=delete_package.php&id=<?php echo $package['package_id']; ?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net@1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-responsive@2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-responsive-bs4@2.2.7/js/responsive.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#packages').DataTable();
});
</script>
