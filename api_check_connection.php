<?php
require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'mikrotik_connection.php';

$username = $_GET['username'];

$client = getMikrotikClient();
$query = (new \RouterOS\Query('/ppp/active/print'))->where('name', $username);
$response = $client->query($query)->read();

$result = [
    'username' => $username,
    'status' => count($response) > 0 ? 'Active' : 'Inactive',
];

header('Content-Type: application/json');
echo json_encode($result);
?>
