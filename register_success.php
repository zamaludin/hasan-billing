<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran Berhasil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .message-container {
            max-width: 600px;
            margin: 50px auto;
            padding: 20px;
            background-color: #ffffff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: center;
        }
        .message-title {
            font-size: 24px;
            margin-bottom: 20px;
            color: #28a745;
        }
        .message-body {
            font-size: 18px;
            margin-bottom: 20px;
        }
        .btn-home {
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <div class="container message-container">
        <h2 class="message-title">Pendaftaran Berhasil</h2>
        <p class="message-body">Terima kasih telah mendaftar layanan ISP kami. Kami akan segera menghubungi Anda untuk informasi lebih lanjut.</p>
        <a href="index.php" class="btn btn-primary btn-home">Kembali ke Beranda</a>
    </div>
</body>
</html>
