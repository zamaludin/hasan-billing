<!DOCTYPE html>
<html>
<head>
    <title>ISP Internet</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background: url('https://source.unsplash.com/1600x900/?internet,technology') no-repeat center center fixed;
            background-size: cover;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            color: #fff;
        }
        .container {
            max-width: 400px;
            padding: 2rem;
            background: rgba(0, 0, 0, 0.7);
            border-radius: 8px;
            box-shadow: 0 4px 6px rgba(0,0,0,0.1);
        }
        .title {
            font-size: 2rem;
            font-weight: bold;
            color: #33AEFF;
            text-align: center;
            margin-bottom: 1rem;
        }
        .form-control {
            border-radius: 50px;
            padding: 1rem;
            font-size: 1rem;
        }
        .btn-primary {
            border-radius: 50px;
            padding: 0.75rem 1.5rem;
            font-size: 1rem;
            background: #33AEFF;
            border: none;
        }
        .btn-primary:hover {
            background: #0062E6;
        }
        .footer {
            text-align: center;
            margin-top: 1rem;
            color: #ccc;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="title">ISP Internet</div>
    <form action="user.php" method="get">
        <div class="form-group">
            <label for="customer_id">Enter Your Customer Number:</label>
            <input type="text" class="form-control" id="customer_id" name="customer_id" placeholder="Customer Number" required>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Check Bill</button>
    </form>
    <div class="footer">© 2024 ISP Internet</div>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
