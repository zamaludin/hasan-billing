<?php
include 'database.php';
$users = getBillingStatus();
?>

<h1 class="my-4">Dashboard Billing</h1>
<table id="customers" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Customer ID</th>
            <th>Name</th>
            <th>Package</th>
            <th>Status</th>
            <th>Location</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo $user['customer_id']; ?></td>
                <td><?php echo $user['name']; ?></td>
                <td><?php echo $user['package_name'] . ' - ' . $user['speed'] . ' - ' . number_format($user['price']); ?></td>
                <td><?php echo $user['status'] ? 'Paid' : 'Unpaid'; ?></td>
                <td><a href="https://maps.google.com/?q=<?php echo $user['lat']; ?>,<?php echo $user['lon']; ?>" target="_blank">View Map</a></td>
                <td>
                    <?php if (!$user['status']): ?>
                        <button class="btn btn-success pay-button" data-id="<?php echo $user['customer_id']; ?>">Pay Billing</button>
                    <?php else: ?>
                        <button class="btn btn-danger cancel-button" data-id="<?php echo $user['customer_id']; ?>">Cancel Payment</button>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<!-- Confirmation Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalLabel">Confirm Action</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p id="confirmMessage">Are you sure you want to perform this action?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary" id="confirmActionButton">Yes</button>
      </div>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net@1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-responsive@2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables.net-responsive-bs4@2.2.7/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/js/adminlte.min.js"></script>
<script>
$.noConflict();

jQuery(document).ready(function($) {
    $('#customers').DataTable();

    var customerId;
    var actionUrl;

    $(document).on('click', '.pay-button', function() {
        customerId = $(this).data('id');
        actionUrl = 'pay_billing.php';
        $('#confirmMessage').text('Are you sure you want to mark this bill as paid?');
        $('#confirmModal').modal('show');
    });

    $(document).on('click', '.cancel-button', function() {
        customerId = $(this).data('id');
        actionUrl = 'cancel_billing.php';
        $('#confirmMessage').text('Are you sure you want to cancel this payment?');
        $('#confirmModal').modal('show');
    });

    $('#confirmActionButton').on('click', function() {
        $.post(actionUrl, { customer_id: customerId }, function(response) {
            location.reload();
        });
    });
});
</script>
