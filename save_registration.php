<?php
include 'database.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = $_POST['name'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $whatsapp = $_POST['whatsapp'];
    $job = $_POST['job'];
    $identity = $_POST['identity'];
    $package_id = $_POST['package_id'];
    $lat = $_POST['lat'];
    $lon = $_POST['lon'];

    // Insert the new customer into the database
    $sql = "INSERT INTO customers (name, birthdate, address, phone, whatsapp, job, identity, package_id, lat, lon, registration_date)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURDATE())";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssssidd", $name, $birthdate, $address, $phone, $whatsapp, $job, $identity, $package_id, $lat, $lon);

    if ($stmt->execute()) {
        header("Location: register_success.php");
        exit();
    } else {
        header("Location: register_error.php");
        exit();
    }
}
?>
