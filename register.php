<?php
include 'database.php';

// Get packages from the database
$sql = "SELECT package_id, name, speed, price FROM packages";
$result = $conn->query($sql);
$packages = $result->fetch_all(MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Layanan ISP</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f8f9fa;
        }
        .register-container {
            max-width: 600px;
            margin: 50px auto;
            padding: 20px;
            background-color: #ffffff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .form-title {
            text-align: center;
            margin-bottom: 20px;
        }
        #map {
            height: 400px;
            width: 100%;
            margin-bottom: 20px;
            position: relative;
        }
        #centerMarker {
            position: absolute;
            background: url('http://maps.gstatic.com/mapfiles/markers2/marker.png') no-repeat;
            top: 50%;
            left: 50%;
            z-index: 1;
            margin-left: -10px;
            margin-top: -34px;
            height: 34px;
            width: 20px;
            pointer-events: none;
        }
        #map:after {
            width: 22px;
            height: 40px;
            display: block;
            content: ' ';
            position: absolute;
            top: 50%; left: 50%;
            margin: -40px 0 0 -11px;
            background: url('https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi_hdpi.png');
            background-size: 22px 40px; /* Since I used the HiDPI marker version this compensates for the 2x size */
            pointer-events: none; /* This disables clicks on the marker. Not fully supported by all major browsers, though */
        }
    </style>
</head>
<body>
    <div class="container register-container">
        <h2 class="form-title">Daftar Layanan ISP</h2>
        <form action="save_registration.php" method="post">
            <div class="form-group">
                <label for="name">Nama:</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="birthdate">Tanggal Lahir:</label>
                <input type="date" class="form-control" id="birthdate" name="birthdate" required>
            </div>
            <div class="form-group">
                <label for="address">Alamat:</label>
                <input type="text" class="form-control" id="address" name="address" required>
            </div>
            <div class="form-group">
                <label for="phone">No. Telp:</label>
                <input type="text" class="form-control" id="phone" name="phone" required>
            </div>
            <div class="form-group">
                <label for="whatsapp">No. WA:</label>
                <input type="text" class="form-control" id="whatsapp" name="whatsapp" required>
            </div>
            <div class="form-group">
                <label for="job">Pekerjaan:</label>
                <input type="text" class="form-control" id="job" name="job" required>
            </div>
            <div class="form-group">
                <label for="identity">No. Identitas:</label>
                <input type="text" class="form-control" id="identity" name="identity" required>
            </div>
            <div class="form-group">
                <label for="package_id">Paket:</label>
                <select class="form-control" id="package_id" name="package_id" required>
                    <?php foreach ($packages as $package): ?>
                        <option value="<?php echo $package['package_id']; ?>">
                            <?php echo $package['name'] . ' - ' . $package['speed'] . 'mbps - Rp ' . number_format($package['price']); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div id="map">
                <div id="centerMarker"></div>
            </div>
            <div class="form-group">
                <label for="lat">Latitude:</label>
                <input type="number" class="form-control" id="lat" name="lat" required readonly>
            </div>
            <div class="form-group">
                <label for="lon">Longitude:</label>
                <input type="number" class="form-control" id="lon" name="lon" required readonly>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Daftar</button>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAL7hFwA0oNkndrOyQml8U-33fE8D5ZFvw&callback=initMap" async defer></script>
    <script>
    $.noConflict();

    function initMap() {
        var initialLocation = { lat: -6.9504887, lng: 107.7860867 }; // Default location (SMPN 4 Jatinangor)
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: initialLocation
        });

        // Set initial latitude and longitude
        document.getElementById('lat').value = initialLocation.lat.toFixed(6);
        document.getElementById('lon').value = initialLocation.lng.toFixed(6);

        // Geolocation to get the current position of the user
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                initialLocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                map.setCenter(initialLocation);
                document.getElementById('lat').value = initialLocation.lat.toFixed(6);
                document.getElementById('lon').value = initialLocation.lng.toFixed(6);
            });
        } else {
            console.warn("Geolocation is not supported by this browser.");
        }

        // Update latitude and longitude values on map drag
        google.maps.event.addListener(map, 'idle', function() {
            var center = map.getCenter();
            document.getElementById('lat').value = center.lat().toFixed(6);
            document.getElementById('lon').value = center.lng().toFixed(6);
        });
    }
    </script>
</body>
</html>
