<?php

require_once 'env_loader.php';
require_once 'vendor/autoload.php';

use RouterOS\Client;
use RouterOS\Config;
use RouterOS\Query;

function getMikrotikClient() { 
    $config = new Config([
        'host' => getenv('MIKROTIK_HOST'),
        'user' => getenv('MIKROTIK_USER'),
        'pass' => getenv('MIKROTIK_PASS'),
        'port' => (int)getenv('MIKROTIK_PORT'),
    ]);
    return new Client($config);
}

function getTrafficData($interface) {
    $client = getMikrotikClient();
    $query = (new Query('/interface/monitor-traffic'))
        ->equal('interface', $interface)
        ->equal('once', '');

    return $client->query($query)->read();
}

function getPPPoEData($retry = 3) {
    $client = getMikrotikClient();
    $query = (new Query('/ppp/secret/print'));

    while ($retry > 0) {
        try {
            return $client->query($query)->read();
        } catch (Exception $e) {
            $retry--;
            if ($retry == 0) {
                throw $e;
            }
        }
    }
}

function getActivePPPoEData($retry = 3) {
    $client = getMikrotikClient();
    $query = (new Query('/ppp/active/print'));

    while ($retry > 0) {
        try {
            return $client->query($query)->read();
        } catch (Exception $e) {
            $retry--;
            if ($retry == 0) {
                throw $e;
            }
        }
    }
}

function getPPPoEUserIP($name) {
    $client = getMikrotikClient();
    $query = (new Query('/ppp/active/print'))
        ->where('name', $name);

    $response = $client->query($query)->read();
    return isset($response[0]['address']) ? $response[0]['address'] : null;
}
?>
