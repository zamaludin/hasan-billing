<?php
require_once 'env_loader.php';

if (!isset($_SESSION)) { 
    session_start(); 
}

$servername = getenv('DB_HOST');
$username = getenv('DB_USERNAME');
$password = getenv('DB_PASSWORD');
$dbname = getenv('DB_NAME');

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function getAllUsers() {
    global $conn;
    $sql = "SELECT * FROM customers";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getBillingStatus() {
    global $conn;
    $sql = "SELECT customers.customer_id, customers.name, packages.name AS package_name, packages.speed, packages.price, 
            IFNULL(bills.status, 0) AS status, customers.lat, customers.lon 
            FROM customers 
            LEFT JOIN bills ON customers.customer_id = bills.customer_id AND bills.month = MONTH(CURRENT_DATE)
            LEFT JOIN packages ON customers.package_id = packages.package_id";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getCustomerById($id) {
    global $conn;
    $sql = "SELECT * FROM customers WHERE customer_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    return $stmt->get_result()->fetch_assoc();
}

function updateCustomer($id, $name, $birthdate, $address, $phone, $whatsapp, $job, $identity, $package_id, $lat, $lon, $registration_date) {
    global $conn;
    $sql = "UPDATE customers SET name = ?, birthdate = ?, address = ?, phone = ?, whatsapp = ?, job = ?, identity = ?, package_id = ?, lat = ?, lon = ?, registration_date = ? 
            WHERE customer_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssssssiddisi", $name, $birthdate, $address, $phone, $whatsapp, $job, $identity, $package_id, $lat, $lon, $registration_date, $id);
    return $stmt->execute();
}

function deleteCustomer($id) {
    global $conn;
    $sql = "DELETE FROM customers WHERE customer_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    return $stmt->execute();
}


function authenticate($username, $password) {
    global $conn;
    $sql = "SELECT * FROM admins WHERE username = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        $user = $result->fetch_assoc();
        return password_verify($password, $user['password']);
    }
    return false;
}

function getAllPackages() {
    global $conn;
    $sql = "SELECT * FROM packages";
    $result = $conn->query($sql);
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getPackageById($id) {
    global $conn;
    $sql = "SELECT * FROM packages WHERE package_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    return $stmt->get_result()->fetch_assoc();
}

function createPackage($name, $speed, $price) {
    global $conn;
    $sql = "INSERT INTO packages (name, speed, price) VALUES (?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssd", $name, $speed, $price);
    return $stmt->execute();
}

function updatePackage($id, $name, $speed, $price) {
    global $conn;
    $sql = "UPDATE packages SET name = ?, speed = ?, price = ? WHERE package_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssdi", $name, $speed, $price, $id);
    return $stmt->execute();
}

function deletePackage($id) {
    global $conn;
    $sql = "DELETE FROM packages WHERE package_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    return $stmt->execute();
}

function createCustomer($name, $birthdate, $address, $phone, $whatsapp, $job, $identity, $package_id, $lat, $lon) {
    global $conn;
    $sql = "INSERT INTO customers (name, birthdate, address, phone, whatsapp, job, identity, package_id, lat, lon)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssssssiddi", $name, $birthdate, $address, $phone, $whatsapp, $job, $identity, $package_id, $lat, $lon);
    return $stmt->execute();
}
function savePPPoEData($data) {
    global $conn;
    $existingUsers = getExistingPPPoEUsers();
    foreach ($data as $pppoe) {
        if (!in_array($pppoe['name'], $existingUsers)) {
            $sql = "INSERT INTO pppoe_users (name, service, profile, comment) VALUES (?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ssss", $pppoe['name'], $pppoe['service'], $pppoe['profile'], $pppoe['comment']);
            $stmt->execute();
        }
    }
}

function getExistingPPPoEUsers() {
    global $conn;
    $sql = "SELECT name FROM pppoe_users";
    $result = $conn->query($sql);
    $users = [];
    while ($row = $result->fetch_assoc()) {
        $users[] = $row['name'];
    }
    return $users;
}

function getAllPPPoEUsers() {
    global $conn;
    $sql = "SELECT * FROM pppoe_users";
    $result = $conn->query($sql);
    return $result->fetch_all(MYSQLI_ASSOC);
}

function pingAddress($ip) {
    $pingResult = exec("ping -c 1 -W 1 " . escapeshellarg($ip), $output, $status);
    return $status === 0;
}
?>

