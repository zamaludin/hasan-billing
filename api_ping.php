<?php
require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'mikrotik_connection.php';

// Ambil data PPPoE dari database
$secrets = getAllPPPoEUsers();
$pingResults = [];

foreach ($secrets as $secret) {
    $username = $secret['name'];
    $ip = getPPPoEUserIP($username);
    if ($ip) {
        $pingResults[$username] = pingAddress($ip) ? 'Active' : 'Inactive';
    } else {
        $pingResults[$username] = 'Inactive';
    }
}

header('Content-Type: application/json');
echo json_encode($pingResults);
?>
